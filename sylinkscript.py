#Process explanation
#Send email containing this script and file that program is supposed to target (Most likely in a zip file to force to extract to a folder)
#User runs script > script runs program and makes program target the file (the file attached along with script in the email)
#Script exists
#!!!! BASH SCRIPT
#Use py2exe to compile script

#Directories the program should be:
#- SEP Client >> C:\Program Files (x86)\Symantec\Symantec Endpoint Protection\<current_install_build_number>\Bin\SylinkDrop.exe.
#- SEPM >> C:\Program Files (x86)\Symantec\Symantec Endpoint Protection\Tools\SylinkDrop\SylinkDrop.exe
#https://support.symantec.com/en_US/article.TECH157585.html

#Apparently code that is working already: https://www.symantec.com/connect/forums/assistance-creating-batch-file-run-sylinkdropexe
#Code already copied and is stored under sym.bat

import os
#import subprocess

attachedFile = "sylink.xml" #File name of attached file

###Check for program directory
SEPMDIR = r"C:\Program Files (x86)\Symantec\Symantec Endpoint Protection\Tools\SylinkDrop\SylinkDrop.exe"
#SEPMDIR = "C:/users/mdy/git/SylinkScript/doesntexist.exe"

def checkClientType():
	isSep = False
	isSepm = False

	if os.path.isfile(SEPMDIR):
		isSepm = True
	else:
		isSep = True

    #print isSepm
    #print isSep

	if isSep:
		programPath = findBuildNumber()
	elif isSepm:
		programPath = SEPMDIR

	return programPath

def findBuildNumber():
    currentDir = "C:\Program Files (x86)\Symantec\Symantec Endpoint Protection"
    #currentDir = "C:/users/mdy/git/SylinkScript/"
    fileName = "SylinkDrop.exe"
    #fileName = "test.bat"
    for root, dirs, files in os.walk(currentDir):
        for name in files:
            #print name
            if name == fileName:
                programPath = os.path.join(root, name)
                return programPath

def runSylink(programPath):
    os.system("%s -silent %s" % (programPath, attachedFile))

if __name__ == '__main__':
    runSylink(checkClientType())
